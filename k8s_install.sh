#! /bin/bash

set -e
set -v

#参考: https://www.ywbj.cc/?p=671
# 禁止swap分区(K8s的要求，确保禁止掉swap分区，不禁止，初始化会报错)
swapoff -a
sed -ri 's/.*swap.*/#&/' /etc/fstab

# 确保时区和时间正确&同时使系统日志时间戳也立即生效
timedatectl set-timezone Asia/Shanghai
systemctl restart rsyslog

# 关闭防火墙和selinux
## ubuntu 查看防火墙命令，ufw status可查看状态，ubuntu20.04默认全部关闭，无需设置

# 主机名和hosts设置(可选)
# cat >> /etc/hosts <<EOF
# 192.168.152.100  k8s-master
# 192.168.152.101  k8s-node01
# 192.168.152.102  k8s-node02
# EOF

# 更改docker默认驱动为systemd(docker有些为cgroupfs, 而kubectl默认驱动为systemd, 需要保持一致)
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"]
}
EOF
systemctl restart docker.service

# 更新 apt 包索引并安装使用 Kubernetes apt 仓库所需要的包
apt-get update
apt-get install -y apt-transport-https ca-certificates curl

# 下载公开签名秘钥、并添加k8s库
curl -s https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | apt-key add -
tee /etc/apt/sources.list.d/kubernetes.list <<EOF
deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main
EOF

# 更新apt包索引, 安装kubelet、kubeadm和kubectl并锁定其版本
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl


# 初始化主节点master
#参考: https://zhuanlan.zhihu.com/p/602626745
kubeadm init --apiserver-advertise-address=192.168.152.100 --pod-network-cidr=172.16.0.0/16
