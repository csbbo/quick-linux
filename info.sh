#! /bin/bash
set -v

# 查看内核/操作系统/CPU信息的
uname -a

# 操作系统版本信息
lsb_release -a

# cpu 核数
cat /proc/cpuinfo| grep "cpu cores" uniq

# 总内存
grep MemTotal /proc/meminfo

# 空闲内存
grep MemFree /proc/meminfo

# 磁盘空间
df -h

# 所有监听端口
netstat -lntp
