# Quick Linux

## 初始化ubuntu22.04 arm架构
```shell
sudo bash -c "$(curl https://gitlab.com/csbbo/quick-linux/-/raw/master/init_ubuntu22.04.sh)"
```

## 查看Linux基本信息
```shell
sudo bash -c "$(curl https://gitlab.com/csbbo/quick-linux/-/raw/master/info.sh)"
```
